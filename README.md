# OZI Project pipeline

OZI pure python packaging components for GitLab CI/CD:

1. checkpoint
2. release
3. publish

## License

The OZI pipeline is licensed Apache-2.0 with LLVM exceptions.

## Project status

Work in Progress
